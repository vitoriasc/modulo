# AT03 - M�dulo de Cadastro de Documentos

**Projeto final de Linguangem de Programa��o II**.    
Este Reposit�rio consiste em disponibilizar um m�dulo para cadastro de documentos atrav�s de input de arquivo.

-------------
 
###Como utilizar ?

1. Copie o link e clone o reposit�rio.

2. Abra o phpmyadmin, crie um banco de dados chamado lp2_modulo e importe o arquivo docs.sql situado na pasta sql.

3. Abra o seu navegador e digite **http://localhost/modulo/docs** em sua barra de pesquisa. 

-------------

###Testes

- Os testes unit�rios podem ser verificados atrav�s da URL **http://localhost/modulo/docs/documento/test/all**

- Os documentos para os testes de aceita��o se encontram na pasta **modulo/doc/selenium/test/src**

-------------
####Desevolvedora 

Vit�ria Sim�es Antonia de Carvalho  
GU3002471  
ADS - 3� semeste  

-------------