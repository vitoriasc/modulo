<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/documento/libraries/DocumentoUsuario.php';
include_once APPPATH . 'modules/documento/controllers/test/builder/DocumentoUsuarioDataBuilder.php';

class DocumentoUsuarioTest extends Toast{
    private $builder;
    private $documento;

    function __construct(){
        parent::__construct('DocumentoUsuarioTest');
    }

    function _pre(){
        $this->builder = new DocumentoUsuarioDataBuilder();
        $this->documento = new DocumentoUsuario();
    }

    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->documento, "Erro na criação do documento");
    }

    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo', $s, 'Erro na seleção do banco de teste');
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $docs = $this->documento->get();
        $this->_assert_equals(3, sizeof($docs), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $doc = $this->documento->get(array('nome' => 'Curriculo_vi', 'usuario_id' => 1))[0];
        $this->_assert_equals('Curriculo_vi', $doc['nome'], "Erro no nome");
        $this->_assert_equals(1, $doc['usuario_id'], "Erro no id do usuario");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        $doc1 = $this->documento->get(array('id' => 2))[0];
        $this->_assert_equals('RG da Mirela', $doc1['nome'], "Erro no nome");
        $this->_assert_equals(2, $doc1['usuario_id'], "Erro no id do usuario");

        $doc1['nome'] = 'RG da Mirela';
        $doc1['usuario_id'] = 2;
        $this->documento->insert_or_update($doc1);

        $doc2 = $this->documento->get(array('id' => 2))[0];
        $this->_assert_equals($doc1['nome'], $doc2['nome'], "Erro no nome");
        $this->_assert_equals($doc1['usuario_id'], $doc2['usuario_id'], "Erro no id do usuario");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $doc1 = $this->documento->get(array('id' => 2))[0];
        $this->_assert_equals('RG da Mirela', $doc1['nome'], "Erro no nome");
        $this->_assert_equals(2, $doc1['usuario_id'], "Erro no id do usuario");

        $this->documento->delete(array('id' => 2));

        $doc2 = $this->documento->get(array('id' => 2));
        $this->_assert_equals_strict(0, sizeof($doc2), "Registro não foi removido");
    }

}