<?php
include_once APPPATH.'/controllers/test/builder/TestDataBuilder.php';

class DocumentoUsuarioDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo'){
        parent::__construct('documento_usuario', $table);
    }

    function getData($index = -1){
        $data[0]['nome'] = 'Curriculo_vi';
        $data[0]['arquivo'] = 'Curriculo_vitoria.pdf';
        $data[0]['tipo_arquivo'] = 'PDF';
        $data[0]['tipo_documento'] = 'Curriculo';
        $data[0]['usuario_id'] = '1';

        $data[1]['nome'] = 'RG da Mirela';
        $data[1]['arquivo'] = 'RG.png';
        $data[1]['tipo_arquivo'] = 'png';
        $data[1]['tipo_documento'] = 'Imagem';
        $data[1]['usuario_id'] = '2';

        $data[2]['nome'] = 'CPF Roberto';
        $data[2]['arquivo'] = 'CPF.png';
        $data[2]['tipo_arquivo'] = 'png';
        $data[2]['tipo_documento'] = 'Imagem';
        $data[2]['usuario_id'] = '3';

        return $index > -1 ? $data[$index] : $data;
    }

}