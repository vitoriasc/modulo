<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/documento/libraries/Usuario.php';


class UsuarioTest extends Toast{
    private $usuario;

    function __construct(){
        parent::__construct('UsuarioTest');
    }

    function _pre(){
        $this->usuario = new Usuario();
    }

    function test_carrega_lista_de_usuarios(){
        $v = $this->usuario->lista();
        $this->_assert_equals(4, sizeof($v), "Número de usuarios incorreto");
    }

    function test_gera_nome_dos_usuarios(){
        $nome = $this->usuario->nome(1);
        $this->_assert_equals('Roberto Justus', $nome, "Erro no nome do usuario");
    }

}