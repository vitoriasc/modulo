<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Documento extends MY_Controller{

    public function __construct(){
        $this->load->model('DocumentoModel', 'model');
    }

    /**
     * Página que exibe a lista de usuarios.
     */
    public function index(){
        $data['titulo'] = 'Cadastro de documentos';
        $data['rotulo_botao'] = 'Novo Usuário';
        $data['form_subject'] = 'novo_usuario';
        $data['show_form'] = false;
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('fake_form', $data, true);
        $data['lista'] = $this->model->fake_list();

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    /**
     * Página para criação de documentos
     * @param int usuario_id: o id do usuario para a qual será direcionada o docuemento
     */
    public function criar($usuario_id){
        $this->add_script('docs/mascara');
        
        $this->validate_id($usuario_id);
        $usuario = $this->model->nome_usuario($usuario_id);
        $data['show_form'] = $this->model->novo_documento($usuario_id);

        $data['home'] = true;
        $data['titulo'] = "Documentos do Usuário - $usuario";
        $data['rotulo_botao'] = 'Novo Documento';
        $data['form_subject'] = 'novo_documento';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('form_documento', $data, true);
        $data['lista'] = $this->model->lista_documentos($usuario_id);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    /**
     * Página para edição dos documentos
     * @param int documento_id: o id do documento editado
     */
    public function editar($documento_id){
        $this->validate_id($documento_id);
        $usuario_id = $this->model->edita_documento($documento_id);
        $usuario = $this->model->nome_usuario($usuario_id);
        $data['show_form'] = true;

        $data['titulo'] = "Editar Documento do usuário - $usuario";
        $data['rotulo_botao'] = 'Novo Documento';
        $data['form_subject'] = 'novo_documento';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('documento/form_documento', $data, true);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    /**
     * Página para deletar os documentos
     * @param int documento_id: o id do documento excluido
     */
    public function deletar($documento_id){
        $this->validate_id($documento_id);
        $data = $this->model->deleta_documento($documento_id);
        $usuario = $this->model->nome_usuario($data['usuario_id']);

        $data['home'] = true;
        $data['titulo'] = "Remover Documento do Usuário - $usuario";
        $data['nome'] = $data['nome'];
        $data['usuario_id'] = $data['usuario_id'];
        $data['usuario'] = $this->model->nome_usuario($data['usuario_id']);
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $html = $this->load->view('confirm_delete', $data, true);
        $this->show($html);
    }

}