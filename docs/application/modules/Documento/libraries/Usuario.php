<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/Dao.php';

class Usuario extends Dao{

    function __construct(){
        parent::__construct('usuario');
    }
    
    /**
     * Lista estática de usuarios. Usada apenas para simular a geração 
     * de conteúdo no módulo responsável pelo registro dos usuarios.
     * Este conteúdo, naturalmente, deveria vir do bd.
     */
    public function lista(){
        return array(
            array('id' => 1, 'Roberto Justus', '414.004.898-06', 'Conferente Fiscal', 'S'),
            array('id' => 2, 'Mirela Ferro', '505.237.448-09', 'Analista de Projetos', 'N'),
            array('id' => 3, 'Jennifer Morais', '400.155.745-x', 'Programadora Java Jr', 'S'),
            array('id' => 4, 'Paulo Tanaka', '436.154.089-88', 'Assistente de Recursos Humanos', 'S'),
        );
    }
        
    /**
     * Determina o nome de um usuario.
     * @param int usuario_id
     * @return string nome do usuario
     */
    public function nome($usuario_id){
        $v = array('', 'Roberto Justus', 'Mirela Ferro', 'Jennifer Morais', 'Paulo Tanaka');
        return $v[$usuario_id];
    }
}