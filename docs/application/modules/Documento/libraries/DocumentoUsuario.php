<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentoUsuario extends Dao {

    function __construct(){
        parent::__construct('documento_usuario');
    }

    public function insert($data, $table = null) {
        $cols = array('nome', 'arquivo', 'tipo_arquivo', 'tipo_documento', 'usuario_id', 'data');
        $this->expected_cols($cols);

        return parent::insert($data);
    }
}