<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Validator extends CI_Object{

    public function form_documento(){
        $this->form_validation->set_rules('nome', 'nome', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('arquivo', 'arquivo', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('tipo_arquivo', 'tipo_arquivo', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('tipo_documento', 'tipo_documento', 'trim|required|min_length[3]|max_length[60]');
        return $this->form_validation->run();
    }

}