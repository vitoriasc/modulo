' <div class="container mt-5">
    <?= $topo_pagina ?>
    <div class="row mt-5">
        <div class="card col-md-8 mx-auto">
            <div class="card-body">
                <h3 class="card-title">Confirmação de remoção</h3><br>
                <p class="card-text">Deseja, realmente, remover o documento <b>"<?= $nome ?>"</b> ?</p><br>

                <form id="delete-doc-form" method="POST">
                    <input type="hidden" name="delete" value="true">
                    <div class="text-right">
                        <a href="<?= base_url('documento/criar/'.$usuario_id) ?>" class="btn btn-light cancel-btn">Cancelar</a>
                        <a class="delete-btn btn btn-unique" onclick="document.getElementById('delete-doc-form').submit();">
                            Remover
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>