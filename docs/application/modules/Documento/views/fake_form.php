<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="novo_usuario">
    <div class="card">
        <div class="card-header elegant-color-dark white-text text-center py-4"><h4>Dados do Usuário</h4></div>
        <div class="card-body px-lg-5 pt-0">
            <form method="POST" class="text-center p-4">
                <div class="form-row mb-4">
                    <div class="col-md-6 md-form">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col-md-6 md-form">
                        <input type="text" name="CPF" value="<?= set_value('CPF') ?>" class="form-control" placeholder="CPF">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-6 md-form">
                        <input type="text" name="cargo" value="<?= set_value('cargo') ?>" class="form-control mb-4" placeholder="Cargo">
                    </div>
                    <div class="col-md-6 md-form">
                        <input type="text" name="ativo" value="<?= set_value('ativo') ?>" class="form-control" placeholder="Ativo" aria-describedby="defaultRegisterFormPasswordHelpBlock">
                    </div>
                </div>
                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-unique">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>