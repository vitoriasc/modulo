<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="novo_documento">
    <div class="card">
        <div class="card-header elegant-color-dark white-text text-center py-4"><h4>Descrição do Documento</h4></div>
        <div class="card-body px-lg-5 pt-0">
            <form method="POST" class="text-center p-4" id="doc-form"  style="color: #757575;">
                <div class="form-row mb-4">
                    <div class="col-md-6 md-form ">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col-md-6 md-form ">
                        <input type="text" name="tipo_documento" value="<?= set_value('tipo_documento') ?>" class="form-control" placeholder="Tipo do Documento">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-6 md-form ">
                        <input type="text" name="tipo_arquivo" value="<?= set_value('tipo_arquivo') ?>" class="form-control" placeholder="Tipo do Arquivo">
                    </div>
                    <div class="col-md-6 md-form">
                        <input type="file"  required name="arquivo" value="<?= set_value('arquivo') ?>" class="form-control" >
                    </div>
                </div>
                <!--
                <div class="form-row mb-4 md-form ">
                    <div class="col-md-12"><textarea name="descricao" class="form-control md-textarea" id="descricao" rows="4" placeholder="Descrição da Atividade"><?= set_value('descricao') ?></textarea></div>
                </div>
                -->
                <div class="text-center text-white text-md-right ">
                    <a class="btnupload-form btn btn-unique" onclick="document.getElementById('doc-form').submit();">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>
