<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/component/buttons/EditDeleteButtonGroup.php';
include_once APPPATH.'libraries/component/Table.php';

class DocumentoModel extends CI_Model{

    /**
     * Gera a tabela que contém a lista de usuarios
     */
    public function fake_list(){
        $this->load->library('Usuario');
        $data = $this->usuario->lista();
        $header = array('#', 'Nome', 'CPF', 'Cargo', 'Ativo');
        $table = new Table($data, $header);
        $table->action('documento/criar');
        $table->use_white_text();
        $table->set_header_color('black');
        $table->use_border();
        $table->use_hover();
        return $table->getHTML();
    }

    /**
     * Gera a lista dos documentos de um usuario cadastrado no bd
     * @param int usuario_id: o identificador do usuario 
     * @return string: código html da tabela
     */
    public function lista_documentos($usuario_id){
        $header = array('#', 'Nome', 'Arquivo', 'Tipo de arquivo', 'Tipo do documento', 'Nome do usuário');
        $this->load->library('DocumentoUsuario', null, 'documento');
        $this->documento->cols(array('id', 'nome', 'arquivo', 'tipo_arquivo', 'tipo_documento', 'data'));
        $data = $this->documento->get(array('usuario_id' => $usuario_id));
        if(! sizeof($data)) return '';
        
        $table = new Table($data, $header);
        $table->use_white_text();
        $table->set_header_color('black');
        $table->use_border();
        $table->use_hover();
        
        $edbg = new EditDeleteButtonGroup('documento');
        $table->use_action_button($edbg);
        return $table->getHTML();
    }

    /**
     * Registra um documento no bd, e pega a data atual do sistema
     * @param $_POST['nome', 'arquivo', 'tipo_arquivo', 'tipo_documento', 'data']
     * @return boolean true caso ocorra erro de validação
     */
    public function novo_documento($usuario_id){
        if(! sizeof($_POST)) return;
        $this->load->library('Validator', null, 'valida');

        if($this->valida->form_documento()){
            $this->load->library('DocumentoUsuario', null, 'documento');
            $data = $this->input->post();
            $data['usuario_id'] = $usuario_id;

            $arquivo = $data['arquivo'];
            $extensao = strtolower(substr($data['arquivo'], -4));
            $novo_nome = md5(time()).$extensao;
            $diretorio = "upload/";

            $data['arquivo'] = move_uploaded_file($arquivo, $diretorio.$novo_nome);
            $data['data'] = date('d/m/y');

            $this->documento->insert($data);
        }
        else return true;
    }

    /**
     * Atualiza os dados de um documento
     * @param int domento_id: o identificador do do2cumento
     * @param int id: o identificador do documento | redireciona para página principal
     */
    public function edita_documento($documento_id){
        $this->load->library('DocumentoUsuario', null, 'documento');
        $this->load->library('Validator', null, 'valida');
        $doc = $this->documento->get(array('id' => $documento_id));

        if(sizeof($_POST) && $this->valida->form_documento()){
            $data = $this->input->post();
            $data['id'] = $documento_id;
            $id = $this->docuemento->insert_or_update($data);
            if($id) redirect('documento/criar/'.$doc[0]['usuario_id']);
        }
        else {
            foreach ($doc[0] as $key => $value)
                $_POST[$key] = $value;
            return $_POST['usuario_id'];
        }
    }

    /**
     * Elimina um documento do bd.
     * @param int documento_id: o identificador do documento
     * @param array: os dados do documento | redireciona para página principal
     */
    public function deleta_documento($documento_id) {
        $this->load->library('DocumentoUsuario', null, 'documento');
        $doc = $this->documento->get(array('id' => $documento_id));
        
        if(sizeof($_POST)) {
            if($this->documento->delete(array('id' => $documento_id)))
                redirect('documento/criar/'.$doc[0]['usuario_id']);
        }
        else return $doc[0];
    }

    /**
     * Determina o nome de um usuario.
     * @param int usuario_id
     * @return string nome da usuario
     */
    public function nome_usuario($usuario_id){
        $this->load->library('Usuario');
        return $this->usuario->nome($usuario_id);
    }
}