package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import page.DocumentoPage;
import page.UsuarioPage;

public class UsuarioTest extends BaseTest{

	@Test
	public void selecionaUsuarioParaCriarDocumento() {
		
		UsuarioPage usuarioPage = new UsuarioPage(driver);
		DocumentoPage documentoPage = usuarioPage.selecionaUsuario(1);
		boolean b = DocumentoPage.temNomeUsuario("Roberto Justus");
		assertTrue("Erro na sele��o de usuario", b);
		
		b = ! documentoPage.temDocumentos();
		assertTrue("N�o deveria haver documentos na p�gina", b);
		
		b = documentoPage.formularioOculto();
		assertTrue("O formul�rio de cadastro dos documentos deveria estar oculto", b);

		b = documentosPage.formularioExibido();
		assertTrue("O formul�rio de cadastro dos documentos deveria estar na tela", b);
		
	}

}
