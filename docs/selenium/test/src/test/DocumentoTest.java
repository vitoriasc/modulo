package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import page.DeletePage;
import page.EditPage;
import page.DocumentoPage;

public class DocumentoTest extends BaseTest {

	@Test
	public void criaDocumento() {
		DocumentoPage documentoPage = new DocumentoPage(driver, 2);
		boolean b = documentoPage.criaDocumentos();
		assertTrue("os documentos n�o foram criados", b);
	}
	
	@Test
	public void editaDocumento() {
		int documento_id = 1;
		int usuario_id = 2;
		
		DocumentoPage documentoPage = new DocumentoPage(driver, usuario_id);
		EditPage editPage = documentoPage.editaDocumentos(documento_id);
		documentoPage = editPage.alteraDadosDocumento(usuario_id);
		boolean b = documentoPage.comfirmaEdicao();
		assertTrue("Erro na edi��o de documentos", b);	
	}
	
	@Test
	public void removeDocumento() {
		int documento_id = 1;
		int usuario_id = 3;
		
		DocumentoPage documentoPage = new DocumentoPage(driver, usuario_id);
		DeletePage deletePage = documentoPage.removeDocumento(documento_id);
		documentoPage = deletePage.removeDocumento(usuario_id);
		boolean b = documentoPage.comfirmaRemocao();
		assertTrue("Erro na remo��o de documentos", b);	
	}
	
}
